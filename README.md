# TP Git – Devinette d'un nombre:

## Objectif:

Créer un mini-jeu de devinette en Python en utilisant Git. Le programme choisit un nombre aléatoire entre 1 et 100. L'utilisateur doit deviner le nombre en moins de 10 essais. L’objectif est d’apprendre les bases de la gestion de versions et se familiariser avec un workflow Git simple. Veuillez lire attentivement les consignes avant d’y répondre. N’oubliez pas de rendre votre travail public sur Gitlab.
## Consignes 
1- Créer un nouveau projet sur Gitlab nommé « Devinette d'un nombre » et le cloner sur votre ordinateur.

![](question1.png)

2- Créer dans le dossier cloné un fichier « main.py » et écrire le programme du jeu.

3- Propagez les modifications sur le dépôt distant avec un message de commit clair et précis.

![](question2.png)

4- Créez une nouvelle branche « dev ».

![](question4.png)
5- Développer dans cette branche la possibilité que le programme affiche des indices pour aider l'utilisateur (plus grand, plus petit) et qu’il affiche à la fin après combien d’essais.

![](question5.png)

5- Sauvegarder les modifications sur le dépôt distant.

![](question6.png)

6- Procéder à la fusion de la banche « dev » avec la branche principale.

![](question7.png)
