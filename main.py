import random

def jeu_de_devinette_sans_indices():
    nombre_a_deviner = random.randint(1, 100)
    essais_restants = 5

    print("Bienvenue dans le jeu de devinette !")
    print("Je pense à un nombre entre 1 et 100.")

    while essais_restants > 0:
        print("Il vous reste", essais_restants, "essais.")
        essai = int(input("Devinez le nombre : "))

        if essai == nombre_a_deviner:
            print("Félicitations ! Vous avez deviné le nombre.")
            return

        essais_restants -= 1

    print("Dommage, vous avez épuisé tous vos essais.")
    print("Le nombre à deviner était", nombre_a_deviner)

jeu_de_devinette_sans_indices()
